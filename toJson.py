import csv
import json
from mutagen.mp3 import MP3

songs = []

with open('songs.csv') as csv_file:
    csv_reader = csv.DictReader(csv_file, delimiter=';')
    line_count = 0
    for row in csv_reader:
        if line_count == 0:
            print(f'Column names are {", ".join(row)}')
            line_count += 1
        song = {}
        #song = []
        id = row['id']
        song['id'] = id
        song['duration'] = "-" if id == "148" else MP3(
            'audios/Хазинаи-дил-' + id + '.mp3').info.length
        song['title'] = row['title']
        song['album'] = row['album']
        song['psalm'] = row['psalm']
        text = row['text']
        textContainer = []
        vNum = 0
        for verse in text.split("<br>"):
            verse = verse.strip()
            if("<p><em>Бандгардон:</em></p>" in verse):
                vType = "chorus"
                verse = verse.replace("<p><em>Бандгардон:</em></p>", "")
            else:
                vType = "verse"
                vNum += 1
            verse = verse.replace("<p>", "")
            verse = verse.replace("<li>", "")
            verse = verse.replace("</p>", "\n")
            verse = verse.replace("</li>", "\n")
            textContainer.append(
                {"type": vType, "content": verse, "no": vNum})
        #textContainer.append({"type": "chorus", "content": "test"})
        song['text'] = textContainer
        # print(id)
        #songs[id] = song
        songs.append(song)
        line_count += 1
    print(f'Processed {line_count} lines.')
    # print(songs)

jsonString = json.dumps(songs)
f = open("songs.json", "w")
f.write(jsonString)
f.close()
