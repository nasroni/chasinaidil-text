import json
import time

md = ""

with open('songs.json') as file:
    test = json.load(file)
    md += "# Хазинаи Дил\n"
    for song in test:
        title = song['title']
        no = song['id']
        md += "## " + title + " (" + no + ")\n"
        if song['psalm'] != "false":
            md += "*Забур " + song['psalm'] + "*  \n"
        for verse in song['text']:
            if(verse['type'] == "chorus"):
                md += "**Chorus**  \n"
            else:
                v = verse['no']
                md += "**Verse " + str(v) + "**  \n"
            md += verse['content'].replace("\n", "  \n")
            md += "\n"
        md += "\n"
    file.close()

with open('songs.md', "w") as file:
    file.write(md)
    file.close()
