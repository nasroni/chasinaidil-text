var transmit = "";
var title = "";

function copyToCB() {
    /* Get the text field */
    var copyText = document.getElementById("myInput");

    /* Select the text field */
    copyText.select();

    /* Copy the text inside the text field */
    document.execCommand("copy");
}

function giveCDName(CDNr) {
    const CDName = {
        1: 'Исо омадааст',
        2: 'Чашмаи ҳаёт',
        3: 'Дар шаби охир',
        4: 'Нури ҷаҳон',
        5: 'Барраи Худо',
        6: 'Моҳигир',
        7: 'Шарирон',
        8: 'Худои воҷибулвуҷуд',
        9: 'Парвардигори меҳрубон',
        10: 'Эй Наҷоткор',
        11: 'Ту кӯзагарӣ',
        12: 'Бимон бо ман',
        13: 'Дурӯзаи умрам',
        14: 'Чӯпони некӯ',
        15: 'Худоё, бубахшо',
        16: 'Пирӯзӣ бар марг',
    };
    return CDName[CDNr];
}


function findInfos() {
    transmit = transmit + "psalm;title;album_name;album;id;text\n";
    $('.block').each(function () {
        try { title = $(this).find('.title_track.search_title').html().split('<')[0].replace(/\r?\n|\r/g, ""); } catch{ title = "-" }
        try { albumNr = $(this).find('.search_cd').html().split('-')[0]; } catch{ }
        try { album = giveCDName(albumNr); } catch{ album = "-" }
        try { mp3Array = $(this).find('.audio_datei').attr('src').split('/'); } catch{ }
        try { mp3 = mp3Array[mp3Array.length - 1]; } catch{ mp3 = "-" }
        try { text = $(this).find('.song_text').html().replace(/\r?\n|\r/g, "").replace(";", "") } catch{ text = "notext" }
        song = "false;" + title + ";" + album + ";" + albumNr + ";" + mp3.substring(12, mp3.length - 4) + ";" + text + "\n"
        console.log(song);
        transmit = transmit + song;
    });

}

function saveFile(CSV) {
    var contentType = 'text/csv';
    var csvFile = new Blob([CSV], { type: contentType });

    var a = document.createElement('a');
    a.download = 'my.csv';
    a.href = window.URL.createObjectURL(csvFile);
    a.textContent = 'Download CSV';

    a.dataset.downloadurl = [contentType, a.download, a.href].join(':');

    document.body.appendChild(a);
}

$(function () {
    console.log('works');
    //transmit = $('body').html();

    findInfos();

    // copyToCB();
    saveFile(transmit);
    console.log('copied');
});
